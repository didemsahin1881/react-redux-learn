import axios from "axios";
export const UPDATE_USER = "UPDATE_USER";

export function updateUser(newUser) {
  return {
    type: UPDATE_USER,
    payload: {
      user: newUser,
    },
  };
}

export function getUsers() {
  return async (dispatch) => {
    axios
      .get("http://localhost:3004/users")
      .then((response) => console.log(response.data))
      .catch((error) => console.log(error));
  };
}
