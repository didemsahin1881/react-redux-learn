import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import thunk from "redux-thunk";
import { compose, applyMiddleware, combineReducers, createStore } from "redux";
import { Provider } from "react-redux";
import userReducer from "./reducers/userReducer";
import productReducer from "./reducers/productReducer";

const rootReducer = combineReducers({
  products: productReducer,
  user: userReducer,
});
const allEnhancers = compose(
  applyMiddleware(thunk),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
const store = createStore(rootReducer, {
  products: [
    {
      name: "Samsung",
      type: "TV",
    },
  ],
  user: "Didem",
});
const updateUserAction = {
  type: "userUpdate",
  payload: {
    user: "Update User Name",
  },
  allEnhancers,
};
store.dispatch(updateUserAction);
console.log(store.getState());
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App count={4} />
    </Provider>
  </React.StrictMode>
);

reportWebVitals();
