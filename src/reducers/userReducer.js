import React from "react";
import { UPDATE_USER } from "../actions/users-actions";

function userReducer(state = "", { type, payload }) {
  switch (type) {
    case UPDATE_USER:
      return payload.user;

    default:
      return state;
  }
}

export default userReducer;

//---(middleware) --> fn();
