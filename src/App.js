import { Component } from "react";
import "./App.css";
import { connect } from "react-redux";
import { updateUser, getUsers } from "./actions/users-actions";
class App extends Component {
  constructor(props) {
    super(props);
    this.onUpdateUser = this.onUpdateUser.bind(this);
  }
  onUpdateUser() {
    this.props.onUpdateUser("Didem SAHIN");
  }
  componentDidMount() {
    this.props.onGetUsers();
  }
  render() {
    console.log(this.props);
    return (
      <div className="App">
        <header className="App-header">
          Learn React Redux
          <h2>{this.props.user}</h2>
          <button onClick={this.onUpdateUser}>Change The Name</button>
        </header>
      </div>
    );
  }
}
const mapStateToProps = (state, props) => {
  return {
    ...state,
    myCount: props.count + 2,
  };
};
const mapDispatchToProps = {
  onUpdateUser: updateUser,
  onGetUsers: getUsers,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
